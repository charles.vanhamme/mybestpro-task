<?php

namespace App\DataFixtures;

use App\Entity\Customer;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $Task = (new Task())
                ->setTitle($faker->sentence(rand(2,3)))
                ->setDescription($faker->text())
                ->setStatus($faker->randomElement(['to do', 'in progress', 'done']));

            $manager->persist($Task);
        }

        $manager->flush();
    }
}
